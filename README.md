# ExtractPoints

Extract student homework points from RWTH-Moodle point tables

Internal help command explains how to use program besides of data import

## Import data

Save the html page displaying the table with all submissions per sheet (standard overview table). Download pages for each sheet.
Name the files `1.html`, ..., `10.html`, `w.html`. If files are missing, the programm will crash outputting which file was the first missing one.
The files have to be in the directory where the binary is executed from.
