extern crate itertools;

use itertools::Itertools;
use std::{collections::HashMap, fs, io::stdin};

struct Student {
    points: Vec<Option<f64>>,
    groups: Vec<Option<String>>,
}

impl Student {
    fn new() -> Student {
        Student {
            points: Vec::new(),
            groups: Vec::new(),
        }
    }
}

const SHEETS: [&str; 10] = ["1", "2", "3", "4", "5", "6", "7", "w", "8", "9"]; // TODO add 10
const TOTAL_REGULAR_POINTS: f64 = 200.0;

fn main() {
    println!("Parsing student points...");

    let mut student_map: HashMap<String, Student> = HashMap::new();

    for sheet in SHEETS.iter() {
        let html = fs::read_to_string(format!("{}.html", sheet))
            .expect(&format!("Missing file {}.html", sheet));
        let mut index = html
            .find("<table class=\"flexible table table-striped table-hover generaltable")
            .unwrap();
        // Rows
        while let Some(next) = &html[index..].find("<td class=\"cell c2\"") {
            // Extract name
            let start = index + *next;
            let end = start + html[start..].find("</td>").unwrap();
            let name = match extract_name(&html[start..end]) {
                Some(name) => name,
                None => {
                    index = end + 1;
                    continue;
                }
            };

            // Extract group
            let start = end + html[end..].find("<td class=\"cell c5\"").unwrap();
            let end = start + html[start..].find("</td>").unwrap();
            let group = extract_group(&html[start..end]);

            // Extract points
            let start = end + html[end..].find("<td class=\"cell c6\"").unwrap();
            let end = start + html[start..].find("</td>").unwrap();
            let mut points = extract_points(&html[start..end]);

            // Extract end points
            let start = end + html[end..].find("<td class=\"cell c15\"").unwrap();
            let end = start + html[start..].find("</td>").unwrap();
            let final_points = extract_final_points(&html[start..end]);

            // Check points validity
            if points != final_points {
                println!("WARNING: Student {} has different points ({:?}) and final points ({:?}) in sheet {}. Proceeding with maximum value of both...", name, points, final_points, sheet);
                if let Some(f_points) = final_points {
                    points = match points {
                        Some(p) if p >= f_points => points,
                        Some(p) if p < f_points => Some(f_points),
                        None => Some(f_points),
                        _ => panic!(),
                    }
                }
            }

            // Resulting operations
            if !student_map.contains_key(&name) {
                student_map.insert(name.clone(), Student::new());
            }
            let student = student_map.get_mut(&name).unwrap();
            student.points.push(points);
            if student.groups.iter().find(|x| **x == group).is_none() {
                student.groups.push(group.clone());
            }

            index = end + 1;
        }
    }

    println!("Parsing student points finished");

    loop {
        println!("Enter a command (h for help):");
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();

        match input.trim() {
            "a" => dump_all(&student_map),
            "g" => search_group(&student_map),
            "h" => println!(
                "---HELP PAGE---\n  \
                a - output all students\n  \
                g - search by group\n  \
                h - show help page\n  \
                q - quit\n  \
                s - search by name\n  \
                x - output all students who commited something but got no admission"
            ),
            "q" => break,
            "s" => search(&student_map),
            "x" => search_no_admission(&student_map),
            s => println!("Error: Unknown command: {}", s),
        }
    }

    println!("Bye")
}

fn search_group(student_map: &HashMap<String, Student>) {
    println!(
        "Enter number of group to search for, e.g. \"123\" even if group name is \"Abgabe123\""
    );
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    for name in student_map.keys().sorted() {
        let student = student_map.get(name).unwrap();
        let group_index = student.groups.iter().position(|x| {
            if let Some(group) = x {
                group.contains(&input.trim())
            } else {
                false
            }
        });
        if group_index.is_some() {
            dump_student(name, student);
        }
    }
}

fn search(student_map: &HashMap<String, Student>) {
    println!("Enter (prefix) of name to search for, name shall be of form \"surname, prename\"");
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    for name in student_map
        .keys()
        .sorted()
        .filter(|x| x.to_lowercase().contains(&input.trim().to_lowercase()))
    {
        let student = student_map.get(name).unwrap();
        dump_student(name, student);
    }
}

fn search_no_admission(student_map: &HashMap<String, Student>) {
    for name in student_map.keys().sorted() {
        let student = student_map.get(name).unwrap();
        let points = student.points.iter().fold(0.0, |accu, x| {
            accu + match x {
                Some(val) => *val,
                None => 0.0,
            }
        });
        if points > 0.0 && points < 0.5 * TOTAL_REGULAR_POINTS {
            dump_student(name, student);
        }
    }
}

fn dump_student(name: &str, student: &Student) {
    println!("Student {}", name);
    println!("    Points: {:?}", student.points);
    println!("    Groups: {:?}", student.groups);
    let points = student.points.iter().fold(0.0, |accu, x| {
        accu + match x {
            Some(val) => *val,
            None => 0.0,
        }
    });
    println!(
        " => {} points ({} % ) in {} submissions over {} groups",
        points,
        points * 100.0 / TOTAL_REGULAR_POINTS,
        student.points.iter().fold(0, |accu, x| accu
            + match x {
                Some(_) => 1,
                None => 0,
            }),
        student.groups.iter().fold(0, |accu, x| accu
            + match x {
                Some(_) => 1,
                None => 0,
            }),
    );
}

fn dump_all(student_map: &HashMap<String, Student>) {
    for name in student_map.keys().sorted() {
        let student = student_map.get(name).unwrap();
        dump_student(name, student);
    }
}

fn extract_cell_content(cell: &str) -> &str {
    let pre_end = cell
        .find(">")
        .expect(&format!("Extracting cell content failed for cell {}", cell));
    &cell[(pre_end + 1)..]
}

fn extract_name(cell: &str) -> Option<String> {
    let inner = extract_cell_content(cell);
    if inner.len() == 0 || inner.chars().next().unwrap() != '<' {
        println!("Detected empty row");
        return None;
    }
    let start = inner
        .find(">")
        .expect(&format!("Error: Name cell content was .{}.", inner))
        + 1;
    let end = start
        + inner[start..]
            .find("</a>")
            .expect(&format!("Error: Name cell content was {}", inner));
    Some(String::from(inner[start..end].trim()))
}

fn extract_group(cell: &str) -> Option<String> {
    let inner = extract_cell_content(cell);
    if inner
        .chars()
        .next()
        .expect(&format!("Error: Group cell content was {}", inner))
        == 'N'
    {
        return None;
    }
    Some(String::from(inner.trim()))
}

fn extract_points(cell: &str) -> Option<f64> {
    let inner = extract_cell_content(cell);
    let start = inner
        .find("<br>")
        .expect(&format!("Error: Point cell content was {}", inner))
        + 4;
    if inner[start..].chars().next().unwrap() == '-' {
        return None;
    }
    let end = start
        + inner[start..]
            .find("&nbsp;")
            .expect(&format!("Error: Point cell content was {}", inner));
    Some(
        inner[start..end]
            .trim()
            .replace(",", ".")
            .parse::<f64>()
            .unwrap(),
    )
}

fn extract_final_points(cell: &str) -> Option<f64> {
    let inner = extract_cell_content(cell);
    if inner.chars().next().unwrap() == '-' {
        return None;
    }
    let end = inner
        .find("&nbsp;")
        .expect(&format!("Error: Final point cell content was {}", inner));
    Some(
        inner[..end]
            .trim()
            .replace(",", ".")
            .parse::<f64>()
            .unwrap(),
    )
}
